# Wizline challenge
This repo has the solution presented by `Cristian Zapata` to the challenge.

The predictions for the blind test set are in the `target_pre.csv` file.

The code solution is presented in the `challenge_solution.ipynb` file.

### How to open this solution?

To open the `Jupyter-Notebook` with the solution, you can use any of the following: 

- If you already have **docker** on your local machine, you can follow the next steps: 
 1. In the `shell` of the respective OS, navegate to the repo folder.
 2. Type and execute the following command: 
  `docker build ./ -t challenge_cristian`. This command builds the respective docker image from the `Dockerfile`.
 4. Now, execute the following command: `docker run -p 8888:8888 challenge_cristian`.
 5. The last command should start a jupyter local server at the 8888 port; copy the URL shown in the `shell` and paste it in the preferred browser.
 6. Now, open the `challenge_solution.ipynb`.

